// 1) Створіть об'єкт product з властивостями name, price та discount. 
// Додайте метод для виведення повної ціни товару з урахуванням знижки. 
// Викличте цей метод та результат виведіть в консоль.

const product = {
    name: 'Headphones',
    price: 1000,
    discount: 0.25,
};

console.log(product);

product.fullPrice = function() {
    return this.price * (1-this.discount);
};

console.log(product.fullPrice());