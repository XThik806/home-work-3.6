// 2) Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
// наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
// за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.

let name, age;
name = prompt("Enter your name: ");
age = prompt("Enter your age: ");

const usercreator = (name, age) => {
    const user = {
        name,
        age,
    };

    return user;
}

const user = usercreator(name, age);

function display(user)
{
    alert(`Привіт, мене звати ${user.name}, мені ${user.age} років`);
}

console.log(user);

display(user);
